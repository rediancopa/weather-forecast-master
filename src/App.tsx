import React, { Component } from 'react';
import axios from 'axios';

import './App.css';
import SearchBar from './components/SearchBar';
import {NoCityError, DataError} from './components/errors/Errors';
import CityWeather from './components/CityWeather';
import WeatherData from './components/WeatherData';

const cities = require('./data/city.list.json');
const apiKey = require('./data/apikey.json');

const dataStoreURL = 'https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&appid={token}&exclude=minutely,hourly,alerts&units=metric'

interface City {
	name: string,
	lat: number,
	lon: number,
}

interface AppState {
	noCityError: boolean,
	dataError: boolean,
	dataLoaded: boolean,
	cityName: string,
	cityWeatherData: WeatherData[],
}

class App extends Component<{}, AppState> {
	constructor (props: {}) {
		super(props)

		this.state = {
			noCityError: false,
			dataError: false,
			dataLoaded: false,
			cityName: '',
			cityWeatherData: [],
		}

		this.processInput = this.processInput.bind(this)
		this.loadCity = this.loadCity.bind(this)
	}
	
	componentDidMount() {
		const munich = cities.find((city: any) => city.name === "Munich")
		this.loadCity({
			name: munich.name,
			lat: munich.coord.lat,
			lon: munich.coord.lon,
		})
	}

	processInput(input: string) {
		const result = cities.find((city: any) => city.name.toLowerCase().startsWith(input))
		if (!result) {
			this.setState({noCityError: true, dataError: false, dataLoaded: false})
			return
		}
		this.setState({noCityError: false})
		this.loadCity({
			name: result.name,
			lat: result.coord.lat,
			lon: result.coord.lon,	
		})	
	}

	loadCity(city: City) {
		console.log(city)
		const url = dataStoreURL
						.replace(/{lat}/g, city.lat.toString())
						.replace(/{lon}/g, city.lon.toString())
						.replace(/{token}/g, apiKey.token)
		const weatherData: WeatherData[] = []
		axios.get(url).then(response => {
			weatherData.push({
				temp: response.data.current.temp,
				desc: response.data.current.weather[0].main,
				icon: response.data.current.weather[0].icon,	
			})
			for(let i = 0; i < 5; i++) {
				weatherData.push({
					temp: response.data.daily[i].temp.day,
					desc: response.data.daily[i].weather[0].main,
					icon: response.data.daily[i].weather[0].icon,
				})
			}
			this.setState({dataError: false, dataLoaded: true, cityName: city.name, cityWeatherData: weatherData})
		}).catch(error => {
			this.setState({dataError: true, dataLoaded: false})
		})
		// TODO delete
		console.log(weatherData)
	}
		
	render() {
		return (
    		<div className="App">
				<div className="content">
					<SearchBar handleInput={this.processInput} />
					{this.state.noCityError && <NoCityError />}
					{this.state.dataError && <DataError />}
					{!this.state.noCityError && !this.state.dataError && this.state.dataLoaded && <CityWeather name={this.state.cityName} data={this.state.cityWeatherData} />}	
				</div>
    		</div>
  		);
	}
}

export default App;
