import React, { Component } from 'react';

import magnifier from './search.svg';
import './SearchBar.css';

class SearchBar extends Component<{ handleInput: (i: string) => void }, { value: string }> {
	constructor (props: any) {
    	super(props);
    	this.state = {
      		value: '',
    	}

		this.handleChange = this.handleChange.bind(this);
		this.findWeather = this.findWeather.bind(this);
  	}
	
	findWeather() {
		this.props.handleInput(this.state.value);
	}

  	handleChange(event: any) {
		this.setState({value: event.target.value});  
	}

	render() {
		return (
    		<div id='searchBar'>
				<img id='searchIcon' src={magnifier} alt='Magnifier' />
				<input 
					id='searchInput' 
					className='font-medium' 
					onKeyDown={(e) => e.key === 'Enter' && this.findWeather() } 
					value={this.state.value}
					onChange={this.handleChange}
				/>
				<button id='searchButton' className='font-medium' onClick={this.findWeather} >Search</button>
    		</div>
  		);
	}
}

export default SearchBar;
