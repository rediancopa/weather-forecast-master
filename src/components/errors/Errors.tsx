import React from 'react';

import notFoundIcon from './not-found.svg';
import repairIcon from './repair.svg';
import './Error.css';

function NoCityError() {
	return (
			<div className='error-container'>
				<img className='error-img' src={notFoundIcon} alt='No city found!' />
				<p>City not found!</p>
			</div>
	)
}

function DataError() {
	return (
			<div className='error-container'>
				<img className='error-img' src={repairIcon} alt='Data error!' />
				<p>City data cound not be loaded! It may be that your API token is not working.</p>
			</div>
	)
}

export {NoCityError, DataError};
