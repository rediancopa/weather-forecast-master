// Put interface in a new file in order to fix exporting bug
export default interface WeatherData {
	temp: number,
	desc: string,
	icon: string,
}
