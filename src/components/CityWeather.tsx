import React, { Component } from 'react';

import WeatherData from './WeatherData';
import './CityWeather.css';
import locationIcon from './pin.svg';

// Weather icons
import Icon01 from './weathericons/i01.svg'; 
import Icon02 from './weathericons/i02.svg'; 
import Icon03 from './weathericons/i03.svg'; 
import Icon04 from './weathericons/i04.svg'; 
import Icon09 from './weathericons/i09.svg'; 
import Icon10 from './weathericons/i10.svg'; 
import Icon11 from './weathericons/i11.svg'; 
import Icon13 from './weathericons/i13.svg'; 
import Icon50 from './weathericons/i50.svg'; 

interface CityWeatherProps {
	name: string,
	data: WeatherData[],
}

function weatherToIcon(icon: string) {
	if (icon.startsWith('01')) return Icon01;
	if (icon.startsWith('02')) return Icon02;
	if (icon.startsWith('03')) return Icon03;
	if (icon.startsWith('04')) return Icon04;
	if (icon.startsWith('09')) return Icon09;
	if (icon.startsWith('10')) return Icon10;
	if (icon.startsWith('11')) return Icon11;
	if (icon.startsWith('13')) return Icon13;
	if (icon.startsWith('50')) return Icon50;
}

class Today extends Component<{name: string, data: WeatherData}, {}> {
	render() {
		return(
			<div id='weatherToday'>
				<div id='cityDescription'>
					<img id='locationIcon' src={locationIcon} alt='loc' />
					<p id='cityName'>{this.props.name.toUpperCase()}</p>
				</div>
				<div id='weatherTodayData'>
					<p id="weatherTodayTemp">{Math.round(this.props.data.temp)}°</p>
					<div id="weatherTodayVisual">
						<img id="weatherTodayIcon" src={weatherToIcon(this.props.data.icon)} alt={this.props.data.icon} />
						<p id="weatherTodayDescription">{this.props.data.desc}</p>
					</div>
				</div>
			</div>
		)
	}
}

class Day extends Component<{}, {}> {
}

class CityWeather extends Component<CityWeatherProps, {}> {
	render() {
		return (
			<div id='weatherData'>
				<Today name={this.props.name} data={this.props.data[0]} />
			</div>
  		);
	}
}

export default CityWeather;
